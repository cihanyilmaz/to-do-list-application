package com.cihanyilmaz.dao;



import com.cihanyilmaz.model.Login;
import com.cihanyilmaz.model.ToDoItem;
import com.cihanyilmaz.security.CustomAuthenticationProvider;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by Cihan Yılmaz on 7.04.2020.
 */
@Repository
public class ToDoItemDao {

    @PersistenceContext
    public EntityManager entityManager;


    public ToDoItem save(ToDoItem item)
    {
        try {
            //sessionFactory.getCurrentSession().save(user);
            entityManager.persist(item);
            return item;

        } catch (Exception e) {
            System.out.println("incorrect save method");
            return null;
        }

    }

    public List<ToDoItem> all()
    {
        try {
            //Session session = this.sessionFactory.getCurrentSession();
            //List<User> musteriListe = session.createQuery("FROM User").list();
            Criteria criteria = entityManager.unwrap(Session.class).createCriteria(ToDoItem.class);
            List results = criteria.list();
            return results;

        } catch (Exception e) {
            System.out.println("incorrect all method");
            return null;
        }

    }

    public void edit(ToDoItem company)
    {
        try {
            //sessionFactory.getCurrentSession().update(user);
            entityManager.merge(company);

        } catch (Exception e) {
            System.out.println("incorrect edit method");
            e.printStackTrace();

        }

    }

    public ToDoItem findId(int id){
        try {
            return entityManager.find(ToDoItem.class, id);


        } catch (Exception e) {
            System.out.println("incorrect findId method");
            return null;
        }


    }

    public void delete(int id)
    {
        try {
            //sessionFactory.getCurrentSession().delete(findId(id));
            entityManager.remove(findId(id));

        } catch (Exception e) {
            System.out.println("incorrect delete method");
            e.printStackTrace();

        }



    }

    public List<ToDoItem> getItems(String username) {

        // Session session = this.sessionFactory.getCurrentSession();
        // Criteria criteria = session.createCriteria(Travel.class);
        Criteria criteria = entityManager.unwrap(Session.class).createCriteria(ToDoItem.class);
        criteria.add(Restrictions.eq("username",username));

        List results = criteria.list();
        return results;
    }


    public List<ToDoItem> ByStatusItems(boolean status) {


        Login login = new Login();
        //Custom providerdan o anki oturumdan kullan?c? ad? çekiliyor ve dto'nun ilgili alan?na set ediliyor.
        login.setUsername(CustomAuthenticationProvider.getUsername());

        // Session session = this.sessionFactory.getCurrentSession();
        // Criteria criteria = session.createCriteria(Travel.class);
        Criteria criteria = entityManager.unwrap(Session.class).createCriteria(ToDoItem.class);
        criteria.add(Restrictions.eq("username",login.getUsername()));
        criteria.add(Restrictions.eq("status",status));

        List results = criteria.list();
        return results;
    }


    public List<ToDoItem> ByDateItems(String  date) {


        Login login = new Login();
        //Custom providerdan o anki oturumdan kullan?c? ad? çekiliyor ve dto'nun ilgili alan?na set ediliyor.
        login.setUsername(CustomAuthenticationProvider.getUsername());

        // Session session = this.sessionFactory.getCurrentSession();
        // Criteria criteria = session.createCriteria(Travel.class);
        Criteria criteria = entityManager.unwrap(Session.class).createCriteria(ToDoItem.class);
        criteria.add(Restrictions.eq("username",login.getUsername()));
        criteria.add(Restrictions.eq("deadline",date));

        List results = criteria.list();
        return results;
    }




}
