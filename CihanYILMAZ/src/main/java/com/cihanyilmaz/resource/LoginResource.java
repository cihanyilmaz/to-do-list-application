package com.cihanyilmaz.resource;

import com.cihanyilmaz.model.Login;
import com.cihanyilmaz.model.User;
import com.cihanyilmaz.security.CustomAuthenticationProvider;
import com.cihanyilmaz.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 * Created by Cihan Y?lmaz on 4.03.2018.
 */
@Component
@Path("/login")
public class LoginResource {
    @Autowired
    private UserService userService;


    @GET
    @Produces("application/json")
    public Login loginControl() {
        Login login = new Login();
        //Custom providerdan o anki oturumdan kullan?c? ad? �ekiliyor ve dto'nun ilgili alan?na set ediliyor.
        login.setUsername(CustomAuthenticationProvider.getUsername());
        //E?er oturum a�?lmam??sa null d�nebilece?inden bu durum kontrol ediliyor.
        if (login.getUsername() != null) {//e?er oturum a�?lm??sa
            User user = userService.login(login.getUsername());//Username'e g�re kullan?c? nesnesi d�n�yor.
            //login nesnesinin ilgili alanlar?na kullan?c? bilgileri set ediliyor.
            login.setFirstLastName(user.getFirstLastName());
            login.setEnable(user.isEnable());
            login.setRole(user.getRole());
            login.setId(user.getId());


        }
        if (login.getUsername() != null)//Kullan?c? giri? yapt?ysa.
            login.setStatus(true);
        else
            login.setStatus(false);//Kullan?c? giri? yapmad?ysa login durumunu false yap?yoruz.
        return login;
    }


}
