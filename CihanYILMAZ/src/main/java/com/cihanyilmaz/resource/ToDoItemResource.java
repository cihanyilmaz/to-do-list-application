package com.cihanyilmaz.resource;

import com.cihanyilmaz.model.ToDoItem;
import com.cihanyilmaz.service.ToDoItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import java.util.List;

/**
 * Created by Cihan Y?lmaz on 8.04.2020.
 */
@Component
@Path("/todolist")
public class ToDoItemResource {

    @Autowired
    private ToDoItemService toDoItemService;

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public ToDoItem save(ToDoItem travel)
    {
        System.out.print("�alistii save metodu");
        toDoItemService.save(travel);

        return travel;
    }


    @DELETE
    @Path("/{Id}")
    @Produces("text/plain")
    public int delete(@PathParam("Id") int Id)
    {   toDoItemService.delete(Id);

        return Id;

    }


    @PUT
    @Path("/{Id}")
    @Consumes("application/json")
    @Produces("application/json")
    public void edit(@PathParam("Id") int travelId,ToDoItem travel)
    {

        toDoItemService.edit(travel);



    }

    @GET
    @Produces("application/json")
    public List<ToDoItem> all()
    {
        return toDoItemService.all();
    }

    @GET
    @Path("/{Id}")
    @Produces("application/json")
    @Consumes("application/json")
    public ToDoItem findId(@PathParam("Id") int Id){

        return toDoItemService.findId(Id);
    }


    @GET
    @Path("/getbyusername/{name}")
    @Produces("application/json")
    public List<ToDoItem> getItems(@PathParam("name") String username) {

        return toDoItemService.getItems(username);


    }


    @GET
    @Path("/getbystatus/{status}")
    @Produces("application/json")
    public List<ToDoItem> ByStatusItems(@PathParam("status")boolean status) {


        try {
            return   toDoItemService.ByStatusItems(status);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }



    }

    @GET
    @Path("/getbydate/{date}")
    @Produces("application/json")
    public List<ToDoItem> ByDateItems(@PathParam("date") String  date) {

        try {
            return   toDoItemService.ByDateItems(date);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }



    }



    }
