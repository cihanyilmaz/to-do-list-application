package com.cihanyilmaz.resource;

import com.cihanyilmaz.model.User;
import com.cihanyilmaz.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import java.util.List;


@Component
@Path("/user")
public class UserResource {

	@Autowired
	private UserService userService;

	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public User save(User user)
	{

		return userService.save(user);
	}

	@GET
	@Produces("application/json")
	public List<User> all()
	{

		return userService.all();
	}

	@GET
	@Path("/{userId}")
	@Produces("application/json")
	@Consumes("application/json")
	public User findId(@PathParam("userId") int userId){

		return userService.findId(userId);
	}

	@PUT
	@Path("/{userId}")
	@Consumes("application/json")
	@Produces("application/json")
	public void edit(@PathParam("userId") int userId,User user)
	{

		userService.edit(user);



	}




	@GET
	@Path("/username/{username}")
	@Produces("application/json")
	@Consumes("application/json")
	public  List<User> findUsername(@PathParam("username") String username){

		return userService.controlUsername(username);
	}


}
