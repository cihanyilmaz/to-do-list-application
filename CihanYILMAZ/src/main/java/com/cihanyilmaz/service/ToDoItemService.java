package com.cihanyilmaz.service;




import com.cihanyilmaz.dao.ToDoItemDao;
import com.cihanyilmaz.model.ToDoItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Cihan Yılmaz on 7.04.2020.
 */
@Service
@Transactional
public class ToDoItemService {


    @Autowired
    private ToDoItemDao toDoItemDao;

    public ToDoItem save(ToDoItem item)
    {
        try {
            return toDoItemDao.save(item);
        } catch (Exception e) {
            return null;
        }


    }



    public List<ToDoItem> all()
    {
        try {
            return toDoItemDao.all();
        } catch (Exception e) {
            return null;
        }


    }

    public void edit(ToDoItem item)
    {
        try {
            toDoItemDao.edit(item);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public ToDoItem findId(int id){

        try {
            return toDoItemDao.findId(id);
        } catch (Exception e) {
            return null;
        }

    }
    public void delete(int id)
    {
        try {
            toDoItemDao.delete(id);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public List<ToDoItem> getItems(String username) {

        try {
          return   toDoItemDao.getItems(username);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }



    }

    public List<ToDoItem> ByStatusItems(boolean status) {


        try {
            return   toDoItemDao.ByStatusItems(status);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }



    }

    public List<ToDoItem> ByDateItems(String  date) {

        try {
            return  toDoItemDao.ByDateItems(date);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }



    }



    }






