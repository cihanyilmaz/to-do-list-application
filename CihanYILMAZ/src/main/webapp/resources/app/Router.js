define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'moment',
    'spin',
    'app/views/LoginView',
    'app/models/LoginModel',
    'app/models/UserModel',
    'app/views/PanelView',
    'app/collections/TodoItemCollection',
    'app/views/EmptyView',
    'app/views/NewItemView',
    'app/views/NewUserView',

    


], function($, _,Backbone,Handlebars,moment,Spinner,LoginView,LoginModel,UserModel,PanelView,TodoItemCollection,EmptyView,NewItemView,
            NewUserView){

    var Router = Backbone.Router.extend({

        routes: {
            '': 'home',
            'panel':'panel',
            'newItem':'newItem',
            'newUser':'newUser'



           

        },
        initialize: function () {

            this.newItemView =new NewItemView();
            this.loginView=new LoginView();
            this.newUserView =new NewUserView();
            this.panelView=new PanelView.HomeView();



        },

        newUser:function () {
            var that=this;
            that.newUserView.render();

        },

        newItem :function () {

            var that=this;
            that.newItemView.render();

        },


        home:function () {

            var that=this;
           that.loginView.render();
        },

        panel:function () {

            var that=this;


            this.panelView.render();

            var loginUser=new LoginModel();
            loginUser.fetch({
                success:function (m_loginUser) {
                    $("#userButton").append(m_loginUser.toJSON().firstLastName);




            var spinner =new Spinner();
            $('body').after(spinner.spin().el); //body



            var todoItemCollection = new TodoItemCollection({username:m_loginUser.toJSON().username});

            todoItemCollection.fetch({

                success:function (m_travels) {

                    spinner.stop();
                    if (m_travels.size() == 0) {
                        var emptyiew=new EmptyView();

                        emptyiew.model={column:5};

                        $("#travelList").append(emptyiew.render().el);

                    }else{

                        m_travels.each(function (m_travel) {
                            debugger
                            var travelView=new PanelView.HomeEventView();
                            travelView.model=m_travel;

                            // that.travelView.model=m_travel;
                            $("#travelList").append(travelView.render().el);
                            // $("#travelList td:gt(47)").hide();//47





                        })

                    }



                }

            });

                }


            });


        }


    });

    return Router;

});