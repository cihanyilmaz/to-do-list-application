/**
 * Created by Cihan Y?lmaz on 11.04.2020.
 */


/**
 * Created by Cihan Y?lmaz on 10.04.2020.
 */


/**
 * Created by Cihan Y?lmaz on 10.04.2020.
 */

define(['jquery', 'underscore', 'backbone', '../models/ToDoItemModel'], function ($, _, Backbone, ToDoItemModel) {


    var ByDateItems=Backbone.Collection.extend({

        model: ToDoItemModel,
        initialize: function ( options) {
            this.deadline = options.deadline;


        },
        url:function () {
            return "rest/todolist/getbydate/"
                + this.deadline;

        }




    });

    return ByDateItems;



});


