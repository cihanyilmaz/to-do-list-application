/**
 * Created by Cihan Y?lmaz on 10.04.2020.
 */


/**
 * Created by Cihan Y?lmaz on 10.04.2020.
 */

define(['jquery', 'underscore', 'backbone', '../models/ToDoItemModel'], function ($, _, Backbone, ToDoItemModel) {


    var ByStatusItems=Backbone.Collection.extend({

        model: ToDoItemModel,
        initialize: function ( options) {
            this.status = options.status;


        },
        url:function () {
            return "rest/todolist/getbystatus/"
                + this.status;

        }




    });

    return ByStatusItems;



});

