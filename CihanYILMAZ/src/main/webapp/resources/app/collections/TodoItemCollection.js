/**
 * Created by Cihan Y?lmaz on 10.04.2020.
 */

define(['jquery', 'underscore', 'backbone', '../models/ToDoItemModel'], function ($, _, Backbone, ToDoItemModel) {


    var GetTodoItems=Backbone.Collection.extend({

        model: ToDoItemModel,
        initialize: function ( options) {
            this.username = options.username;


        },
        url:function () {
            return "rest/todolist/getbyusername/"
                + this.username;

        }




    });

    return GetTodoItems;



});

