/**
 * Created by Cihan Y?lmaz on 10.04.2020.
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
], function($, _, Backbone,Handlebars){



    var ToDoItemModel=Backbone.Model.extend({

        urlRoot:'/rest/todolist',
        validate:function (ev) {
            if(ev.name.trim()==""){
                return "nameAdd";
            }

            if(ev.description.trim()=="")
            {
                return "descriptionAdd";
            }

            if(ev.deadline.trim()=="")
            {
                return "deadlineAdd";
            }


        }

    });

    return ToDoItemModel;



});


