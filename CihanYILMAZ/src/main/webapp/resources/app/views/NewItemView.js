/**
 * Created by Cihan Y?lmaz on 10.04.2020.
 */


define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'text!app/templates/NewItemTemplate.html',
    'app/models/ToDoItemModel',
    'app/models/LoginModel'

], function($, _,Backbone,Handlebars,NewItemTemplate,ToDoItemModel,LoginModel) {

    var NewItemiew = Backbone.View.extend({

            el: '.page',
            events:{
                'click #saveItem':'new',
                'click #notSaveItem':'cancel'


            },

            new:function () {

                var item=new ToDoItemModel();




                var loginModel=new LoginModel();
                loginModel.fetch({
                    success:function (login) {


                        item.set('name',$("#name").val());
                        item.set('description',$("#description").val());
                        item.set("deadline",$("#deadline").val());
                        item.set('status',0);
                        item.set('username',login.toJSON().username);


                        if(!item.isValid())
                        {

                            $("." + item.validationError).tooltip("show");

                        }else{
                            debugger
                            item.save();
                            window.location.href='#/panel';

                            return false;


                        }


                        return false;

                    }


                });





            },

        cancel:function () {

            window.location.href='#/panel';

            return false;

        },

            render: function () {

                this.$el.html(NewItemTemplate);
                return this;

            }


        }
    );
    return NewItemiew;





});

