package com.cihanyilmaz;

import com.cihanyilmaz.model.ToDoItem;
import com.cihanyilmaz.resource.ToDoItemResource;
import com.cihanyilmaz.service.ToDoItemService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

/**
 * Created by Cihan Y?lmaz on 11.04.2020.
 */

public class ToDoItemResourceTest {


    @Test
    public void testGet() throws URISyntaxException {


        RestTemplate restTemplate = new RestTemplate();
        final String baseUrl = "http://localhost:8080/rest/todolist";
        URI uri = new URI(baseUrl);

        ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);

        //Verify request succeed
        assertEquals(HttpStatus.OK, result.getStatusCode());

    }

    @Test
    public void testDelete() throws URISyntaxException {

        RestTemplate restTemplate = new RestTemplate();
        final String baseUrl = "http://localhost:8080/rest/todolist/3";
        URI uri = new URI(baseUrl);


        restTemplate.delete(uri);


    }

    @Test
    public void testByStatusItems() throws URISyntaxException {

        RestTemplate restTemplate = new RestTemplate();
        final String baseUrl = "http://localhost:8080/rest/todolist/getbystatus/true";
        URI uri = new URI(baseUrl);

        ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);

        //Verify request succeed
        assertEquals(HttpStatus.OK, result.getStatusCode());


    }


    @Test
    public void testByDateItems() throws URISyntaxException {

        RestTemplate restTemplate = new RestTemplate();
        final String baseUrl = "http://localhost:8080/rest/todolist/getbydate/2020-04-16";
        URI uri = new URI(baseUrl);

        ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);

        //Verify request succeed
        assertEquals(HttpStatus.OK, result.getStatusCode());


    }

}