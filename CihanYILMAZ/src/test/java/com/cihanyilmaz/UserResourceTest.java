package com.cihanyilmaz;

import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;

/**
 * Created by Cihan Y?lmaz on 11.04.2020.
 */


public class UserResourceTest {


    @Test
    public void testGet() throws URISyntaxException {


        RestTemplate restTemplate = new RestTemplate();
        final String baseUrl = "http://localhost:8080/rest/user";
        URI uri = new URI(baseUrl);

        ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);

        //Verify request succeed
        assertEquals(HttpStatus.OK, result.getStatusCode());

    }


    @Test
    public void testFindUsername() throws URISyntaxException {

        RestTemplate restTemplate = new RestTemplate();
        final String baseUrl = "http://localhost:8080/rest/user/username/a";
        URI uri = new URI(baseUrl);

        ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);

        //Verify request succeed
        assertEquals(HttpStatus.OK, result.getStatusCode());


    }


}
