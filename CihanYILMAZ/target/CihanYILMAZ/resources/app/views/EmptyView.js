define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'text!app/templates/EmptyTemplate.html'

], function($, _,Backbone,Handlebars,EmptyTravelTemplate) {


    var EmptyView = Backbone.View.extend({
        tagName: "tr",
        model: {defaults: {column:0}},
        render: function () {
            var that = this;
            var template = Handlebars.compile(EmptyTravelTemplate);
            var myHtml = template(that.model);
            that.$el.html(myHtml);
            return this;
        }
    });

    return EmptyView;

});
