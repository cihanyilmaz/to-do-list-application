define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'text!app/templates/NewUserTemplate.html',
    'app/models/UserModel',

], function($, _,Backbone,Handlebars,NewUserTemplate,UserModel) {

    var NewUserView = Backbone.View.extend({

            el: '.page',
        events:{
            'click #new':'new',


        },

        new:function () {

            var user=new UserModel();

            user.set("username",$("#username").val());
            user.set('firstLastName',$("#nameSurname").val());
            user.set('enable',1);
            user.set('password',$("#password").val());
            user.set('role',"tanimsiz");

            if(!user.isValid())
            {

                $("." + user.validationError).tooltip("show");
                return false;

            }else {

                user.save();
                window.location.href='#/';
                return false;


            }






        },

            render: function () {

                this.$el.html(NewUserTemplate);
                return this;

            }


        }
    );
    return NewUserView;





});
