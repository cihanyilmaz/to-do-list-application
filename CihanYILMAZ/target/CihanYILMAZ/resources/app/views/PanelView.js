define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/ToDoItemModel',
    'text!app/templates/PanelTemplate.html',
    'text!app/templates/PanelListTemplate.html',
    'app/collections/SearchItemByStatusCollection',
    'app/views/EmptyView',
    'app/collections/SearchItemByDateCollection',



], function($, _,Backbone,Handlebars,ToDoItemModel, PanelTemplate,PanelListTemplate,SearchItemByStatusCollection,EmptyView,
            SearchItemByDateCollection){

    var HomeEventView=Backbone.View.extend({
        tagName:'tr',
        model:ToDoItemModel,
        events: {
            'click .editTravel': 'editTravel',
            'click .updateTravel': 'updateTravel',
            'click .cancelEdit': 'cancelEdit',
            'click .deleteTravel': 'deleteTravel',
            'mouseover .resultRow':'changeColor',
            'mouseout .resultRow':'notChangeColor'
        },
        notChangeColor:function () {
            this.$el.css('color', 'black');

        },
        changeColor:function () {

            this.$el.css('color', 'red');
        },
        editTravel: function () {
            this.$el.addClass("info");
            this.$el.find("input").show().focus();
            this.$el.find("select").show().focus();
            this.$el.find("span").hide();
            $(".editTravel").hide();
            $(".deleteTravel").hide();
            // this.$el.find(".editTravel").hide();
            // this.$el.find(".deleteTravel").hide();

            this.$el.find(".updateTravel").show();
            this.$el.find(".cancelEdit").show();
        },
        cancelEdit: function () {
            this.$el.removeClass("info");
            $(".editTravel").show();
            $(".deleteTravel").show();
            this.render();
        },
        updateTravel: function () {
            debugger
            var name = this.$el.find("#name").val();
            var description = this.$el.find("#description").val();
            var deadline = this.$el.find("#deadline").val();
            var status = this.$el.find("#status").val();


            var newValues=new ToDoItemModel();
            newValues.set("name",name);
            newValues.set("description",description);
            newValues.set("deadline",deadline);
            newValues.set("status",status);



            this.$el.removeClass("info");
            if (!newValues.isValid()){

                $("." + newValues.validationError).tooltip("show");


            }else{

                this.$el.removeClass("info");
                this.model.set("name", name);
                this.model.set("description", description);
                this.model.set("deadline", deadline);
                this.model.set("status", status);



                this.model.save();




            }
            $(".editTravel").show();
            $(".deleteTravel").show();


            this.render();
        },
        deleteTravel: function () {

            if(uyar())
            {
                this.$el.find(".deleteTravel").addClass("disabled");
                var that = this;

                this.model.destroy({
                    wait: true,
                    success: function () {
                        that.remove();

                    },

                }); // HTTP DELETE

            }
            else{

                return false;


            }

        },
        render: function () {
            var that = this;
            var template = Handlebars.compile(PanelListTemplate);
            var myHtml = template(that.model.toJSON());
            that.$el.html(myHtml);
            return this;
        }




    });
    var HomeView=Backbone.View.extend({

        el: $(".page"),
        events:{
            'click #searchDate': 'searchDate',
            'change #selectStatus': 'selectStatus',





        },
        selectStatus:function () {

            var status = $("#selectStatus").val();

            debugger
            var search = new SearchItemByStatusCollection( {
                status: status

            });



            search.fetch({

                success: function (m_travels) {

                    $(".resultRow").parent().empty();

                    if (m_travels.size() == 0) {
                        var emptyView=new EmptyView();

                        emptyView.model={column:5};

                        $("#travelList").append(emptyView.render().el);


                    }else{

                        m_travels.each(function (m_travel) {


                            var searchView = new HomeEventView();
                            searchView.model = m_travel;
                            $("#travelList").append(searchView.render().el);
                        });




                    }


                }
            });




        },

        searchDate:function () {
            debugger
            var deadline = $("#date").val();
            $("#date").val("");

            var search = new SearchItemByDateCollection( {
                deadline: deadline

            });



            search.fetch({

                success: function (m_travels) {

                    $(".resultRow").parent().empty();

                    if (m_travels.size() == 0) {
                        var emptyView=new EmptyView();

                        emptyView.model={column:5};

                        $("#travelList").append(emptyView.render().el);


                    }else{

                        m_travels.each(function (m_travel) {


                            var searchView = new HomeEventView();
                            searchView.model = m_travel;
                            $("#travelList").append(searchView.render().el);
                        });




                    }


                }
            });

        },



        render: function () {
            this.$el.html(PanelTemplate);
            return this;
        }
    });


    return {
        HomeEventView: HomeEventView,
        HomeView: HomeView
    };

});

